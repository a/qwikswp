import logging
import traceback

import asyncpg
import aiohttp

from sanic import Sanic, exceptions
from sanic import response
from sanic_cors import CORS

import api.bp.get
import api.bp.put

from api.errors import APIError

import config

app = Sanic()
app.econfig = config

# enable cors on get and put
# CORS(app, resources=[r"/get", r"/put"])

# load blueprints
app.blueprint(api.bp.get.bp)
app.blueprint(api.bp.put.bp)

logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__name__)


async def options_handler(request):
    return response.text('ok')


@app.exception(APIError)
def handle_api_error(request, exception):
    """
    Handle any kind of application-level raised error.
    """
    log.warning(f'API error: {exception!r}')
    return response.json({
        'error': True,
        'message': exception.args[0]
    }, status=exception.status_code)


@app.exception(Exception)
def handle_exception(request, exception):
    # how do traceback loge???
    val = traceback.format_exc()
    if 'self._ip' in val:
        return None

    status_code = 500
    if isinstance(exception, (exceptions.NotFound, exceptions.FileNotFound)):
        status_code = 404

    log.exception(f'error in request: {repr(exception)}')
    return response.json({
        'error': True,
        'message': repr(exception)
    }, status=status_code)


@app.listener('before_server_start')
async def setup_db(app, loop):
    """Initialize db connection before app start"""
    app.session = aiohttp.ClientSession(loop=loop)

    log.info('connecting to db')
    app.db = await asyncpg.create_pool(**config.db)
    log.info('connected to db')


def main():
    # "fix" CORS.
    #routelist = list(app.router.routes_all.keys())
    #for uri in list(routelist):
    #    try:
    #        app.add_route(options_handler, uri, methods=['OPTIONS'])
    #    except:
    #        pass

    app.run(host=config.HOST, port=config.PORT)

if __name__ == '__main__':
    main()
