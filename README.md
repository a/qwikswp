qwikswp
=======

A FOSS project to allow text content to be shared across the people with the same IP, similar to ssavr.com, but simpler, open source and less UI based. It's just some code quickly stuck together.


#### Installation
- Install python3.6
- Install postgresql (I use 10.x)
- Add a user and a DB to postgresql, import the schema
- Copy `config.py.example` to `config.py`, configure it with DB info and other stuff.
- Install dependencies with pip
- Run the `run.py` file


#### Usage

I recommend using IPv4 unless you're sharing to the same device.

There is one GET endpoint and one POST endpoint:

###### POST /set

Takes a json request with `text` set to the text you want to be stored, returns `error` or `success` set to true. If it is an error, it also includes a `message` field.


###### GET /get

If there's anything stored for your IP, it'll return the message. 

If there isn't anything stored, returns `error` set to `true`, with a `message` field.


#### Using with your fave shell

###### bash/zsh

Put this in your .zshrc/.bashrc (yes, it's a little hacky, but eh)

```bash
qwikset()
{
    if read -t 0; then
            stdin=$(cat)
    else
            stdin=$*
    fi
    curl -H "Content-Type: application/json" -X POST -d "{\"text\":\"$stdin\"}" https://<your qwikswp instance>/set -4
}

qwikget()
{
    curl https://<your qwikswp instance>/get -4
}
```

###### fish

no. 