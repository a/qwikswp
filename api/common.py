async def get_ip(request) -> str:
    use_cf = request.app.econfig.CF_PROXIED
    if use_cf and "cf-connecting-ip" in request.headers:
        return request.headers["cf-connecting-ip"]
    else:
        return request.ip
