import logging

from sanic import Blueprint
from sanic import response

from ..errors import BadInput
from ..common import get_ip

bp = Blueprint('put')
log = logging.getLogger(__name__)


@bp.post('/set')
async def set_handler(request):
    """
    True hell happens in this function.

    We need to check a lot of shit around here.

    If things crash, we die.
    """
    user_ip = await get_ip(request)
    inc_data = request.json

    if not inc_data or "text" not in inc_data:
        raise BadInput(f"You didn't give any input.")

    swp = inc_data["text"]
    swpsize = len(swp)
    maxsize = request.app.econfig.SIZE_LIMIT

    if maxsize != -1 and swpsize > maxsize:
        raise BadInput(f"Input is too big.")

    # Let's first clear up any previous stuff of this user
    await request.app.db.execute("""
    DELETE FROM ipcontent
    WHERE ip = $1
    """, user_ip)

    # then let's add their content
    await request.app.db.execute("""
    INSERT INTO ipcontent (ip, content)
    VALUES ($1, $2)
    """, user_ip, swp)

    return response.json({'success': True})
