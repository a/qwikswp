import logging

from sanic import Blueprint
from sanic import response

from ..errors import NotFound
from ..common import get_ip

bp = Blueprint('get')
log = logging.getLogger(__name__)


@bp.get('/get')
async def get_handler(request):
    """Handles /get requests for qwikswps."""
    user_ip = await get_ip(request)
    content = await request.app.db.fetchval("""
    SELECT content
    FROM ipcontent
    WHERE ip = $1
    """, user_ip)

    if not content:
        raise NotFound(f"There's nothing saved on this IP ({user_ip}).")

    return response.text(content)
