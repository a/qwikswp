CREATE TABLE IF NOT EXISTS ipcontent (
    ip text PRIMARY KEY NOT NULL,
    change timestamp without time zone default (now() at time zone 'utc'),
    content text,

    deleted boolean DEFAULT false
);